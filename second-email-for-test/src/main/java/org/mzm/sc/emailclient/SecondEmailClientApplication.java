package org.mzm.sc.emailclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SecondEmailClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondEmailClientApplication.class, args);
    }

}
