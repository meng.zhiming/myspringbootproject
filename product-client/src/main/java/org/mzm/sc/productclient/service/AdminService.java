package org.mzm.sc.productclient.service;

import org.mzm.sc.productclient.dao.AdminRoleMapper;
import org.mzm.sc.productclient.model.ProductInfo;
import org.mzm.sc.productclient.util.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AdminService {

    private final static Logger logger = LoggerFactory.getLogger(AdminService.class);

    private AdminRoleMapper adminRoleDao;

    @Autowired
    public void setAdminRoleDao(AdminRoleMapper adminRoleDao) {
        this.adminRoleDao = adminRoleDao;
    }

    @Transactional(rollbackFor = Exception.class)
    public ActionResponse insertNewProduct(ProductInfo product) {
        logger.info(String.format("insert new product %s", product.getProductName()));
        ActionResponse ar = new ActionResponse();

        int result = 0;
        try {
            result = adminRoleDao.insertNewProduct(product);
        } catch (Exception e) {
            logger.error("Catch exception when insert new product: " + e.getMessage());
            ar.addErrorResponse("Internal service error");
        }

        if (result == 0) {
            logger.error("No row insert for new product");
            ar.addErrorResponse("Fail for insert new product");
        }

        return ar.addSuccessResponse("Save new product success");
    }

}
