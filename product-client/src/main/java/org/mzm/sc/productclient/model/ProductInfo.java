package org.mzm.sc.productclient.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.mzm.sc.productclient.util.ColorEnum;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProductInfo {

    private Integer id;
    // 商品编码
    private String productCore;
    private String productName;
    //'国条码'
    private String barCode;
    //'品牌表的ID'
    private Integer brandId;
    // 分类ID
    private int oneCategoryId;
    private int twoCategoryId;
    private int threeCategoryId;
    //商品的供应商ID
    private int supplierId;
    private BigDecimal price;
    //'商品加权平均成本'
    private BigDecimal averageCost;
    //'上下架状态：0下架1上架'
    private Integer publishStatus;
    //'审核状态：0未审核，1已审核'
    private Integer auditStatus;
    private Float weight;
    private Float length;
    private Float height;
    private Float width;
    private ColorEnum colorType;
    private Timestamp productionDate;
    //'商品有效期'
    private Integer shelfLife;
    private String descript;
    //'商品录入时间'
    private Timestamp indate;
    //'最后修改时间'
    private Timestamp modifiedTime;
}
