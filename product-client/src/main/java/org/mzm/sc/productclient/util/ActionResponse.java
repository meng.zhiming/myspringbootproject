package org.mzm.sc.productclient.util;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ActionResponse {

    public enum ActionState {
        /**
         * ActionState SUCCESS
         **/
        SUCCESS,

        /**
         * ActionState FAIL
         **/
        FAIL,
        /**
         * ActionState ERROR
         **/
        ERROR
    }

    private ActionState actionState = ActionState.SUCCESS;
    private List<String> responseMessages = new ArrayList<>();

    public ActionState getActionState() {
        return actionState;
    }

    public void setActionState(ActionState actionState) {
        this.actionState = actionState;
    }

    public ActionResponse addErrorResponse(String errorMessage) {
        responseMessages.add(errorMessage);
        actionState = ActionState.ERROR;
        return this;
    }

    public ActionResponse addSuccessResponse(String successMessage) {
        responseMessages.add(successMessage);
        actionState = ActionState.SUCCESS;
        return this;
    }

    public boolean hasError() {
        return !responseMessages.isEmpty() && ActionState.ERROR.equals(actionState);
    }

    public List<String> getResponseMessages() {
        return responseMessages;
    }

    public void setResponseMessages(List<String> responseMessages) {
        this.responseMessages = responseMessages;
    }
}
