package org.mzm.sc.productclient.controller;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.mzm.sc.productclient.model.ProductInfo;
import org.mzm.sc.productclient.service.GuestRoleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product")
public class GuestRoleController {

    @Resource
    GuestRoleService guestRoleService;

    @RequestMapping(value = "getProductList", method = RequestMethod.GET)
    public List<Map<String, Object>> getProductList() {
        return guestRoleService.getProductList();
    }

    @RequestMapping(value = "getProductDetailById", method = RequestMethod.GET)
    public ProductInfo getProductDetailById(Integer id) {
        return guestRoleService.getProductDetailById(id);
    }
}
