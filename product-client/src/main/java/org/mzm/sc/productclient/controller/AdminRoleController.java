package org.mzm.sc.productclient.controller;

import javax.annotation.Resource;
import org.mzm.sc.productclient.model.ProductInfo;
import org.mzm.sc.productclient.service.AdminService;
import org.mzm.sc.productclient.util.ActionResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/product")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminRoleController {

    @Resource
    private AdminService adminService;

    @RequestMapping(value = "/addNewProduct", method = RequestMethod.POST)
    public ActionResponse addNewProduct(ProductInfo product) {
        return adminService.insertNewProduct(product);
    }
}
