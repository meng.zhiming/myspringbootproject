package org.mzm.sc.productclient.dao;

import org.mzm.sc.productclient.model.ProductInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRoleMapper {

    int insertNewProduct(ProductInfo product);
}
