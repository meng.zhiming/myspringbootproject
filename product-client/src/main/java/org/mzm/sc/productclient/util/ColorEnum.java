package org.mzm.sc.productclient.util;

public enum ColorEnum {

    red("red"), yellow("yellow"), blue("blue"), black("black");

    private String name;

    ColorEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static ColorEnum fromString(String displayName) {
        if (displayName != null) {
            for (ColorEnum sectionEnum : ColorEnum.values()) {
                if (displayName.equalsIgnoreCase(sectionEnum.getName())) {
                    return sectionEnum;
                }
            }
        }
        return null;
    }
}
