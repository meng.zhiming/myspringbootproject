package org.mzm.sc.productclient.service;

import java.util.List;
import java.util.Map;
import org.mzm.sc.productclient.dao.GuestRoleMapper;
import org.mzm.sc.productclient.model.ProductInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GuestRoleService {

    private static final Logger logger = LoggerFactory.getLogger(GuestRoleService.class);

    GuestRoleMapper guestRoleDao;

    @Autowired
    public void setGuestRoleDao(GuestRoleMapper guestRoleDao) {
        this.guestRoleDao = guestRoleDao;
    }

    public List<Map<String, Object>> getProductList() {
        logger.info("Get all products");
        return guestRoleDao.getProductList();
    }

    public ProductInfo getProductDetailById(Integer id) {
        logger.info(String.format("Get product detail by id: %s", id));
        return guestRoleDao.getProductDetailById(id);
    }
}
