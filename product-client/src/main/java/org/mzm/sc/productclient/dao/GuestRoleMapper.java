package org.mzm.sc.productclient.dao;

import java.util.List;
import java.util.Map;
import org.mzm.sc.productclient.model.ProductInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface GuestRoleMapper {
    List<Map<String, Object>> getProductList();

    ProductInfo getProductDetailById(Integer id);
}
