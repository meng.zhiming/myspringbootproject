package org.mzm.sc.warehouseclient;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(value = "org.mzm.sc.warehouseclient.dao")
@EnableDistributedTransaction
public class WarehouseClientApplication {

  public static void main(String[] args) {
    SpringApplication.run(WarehouseClientApplication.class, args);
  }
}
