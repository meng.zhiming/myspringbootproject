package org.mzm.sc.warehouseclient.dao;

import org.springframework.stereotype.Repository;

@Repository
public interface StockMapper {

    int getStockNumberByProductId(Integer productId);

    int reduceStock(Integer purchaseNum, Integer productId);

    int preReduceStockAndLock(Integer purchaseNum, Integer productId);

    int confirmReduceStock(Integer purchaseNum, Integer productId);

    int rollbackReduceStock(Integer purchaseNum, Integer productId);
}
