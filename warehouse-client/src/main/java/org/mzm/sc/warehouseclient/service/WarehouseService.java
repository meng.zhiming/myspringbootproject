package org.mzm.sc.warehouseclient.service;

import com.codingapi.txlcn.tc.annotation.DTXPropagation;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.codingapi.txlcn.tc.annotation.TccTransaction;
import com.codingapi.txlcn.tc.annotation.TxcTransaction;
import org.mzm.sc.warehouseclient.dao.StockMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class WarehouseService {

    private static final Logger logger = LoggerFactory.getLogger(WarehouseService.class);

    @Autowired
    private StockMapper stockDao;

    public int getStockNumberByProductId(Integer productId) {
        return stockDao.getStockNumberByProductId(productId);
    }

    @Transactional
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    public Integer preReduceStockLCN(Integer purchaseNum, Integer productId) {
        // TODO: here need lock, will use redis lock.
        int stockNum = this.getStockNumberByProductId(productId);

        logger.info(String.format("There are %d stocks for product: %s", stockNum, productId));
        if (stockNum < purchaseNum) {
            return 0;
        }

        return stockDao.reduceStock(purchaseNum, productId);
    }


    /**
     * TXC模式命名来源于淘宝，实现原理是在执行SQL之前，先查询SQL的影响数据，然后保存执行的SQL快走信息和创建锁。
     * 当需要回滚的时候就采用这些记录数据回滚数据库，目前锁实现依赖redis分布式锁控制。
     **/
    @Transactional
    @TxcTransaction(propagation = DTXPropagation.SUPPORTS)
    public Integer preReduceStockTXC(Integer purchaseNum, Integer productId) {
        return stockDao.reduceStock(purchaseNum, productId);
    }

    @Transactional
    @TccTransaction(propagation = DTXPropagation.SUPPORTS)
    public Integer preReduceStockTCC(Integer purchaseNum, Integer productId) {
        return stockDao.preReduceStockAndLock(purchaseNum, productId);
    }

    public void confirmPreReduceStockTCC(Integer purchaseNum, Integer productId) {
        logger.info("confirm for preReduceStockTCC");
        int status = stockDao.confirmReduceStock(purchaseNum, productId);
        logger.info("status for confirm reduce stock: " + status);
    }

    public void cancelPreReduceStockTCC(Integer purchaseNum, Integer productId) {
        logger.info("cancel for preReduceStockTCC");
        int status = stockDao.rollbackReduceStock(purchaseNum, productId);
        logger.info("status for rollback reduce stock: " + status);
    }
}
