package org.mzm.sc.warehouseclient.controller;

import org.mzm.sc.warehouseclient.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/warehouse")
public class WarehouseController {

  @Autowired
  private WarehouseService service;

  @RequestMapping(value = "/getStockNumberByProductId", method = RequestMethod.GET)
  public int getStockNumberByProductId(Integer productId) {
    return service.getStockNumberByProductId(productId);
  }

  // for feign
  @RequestMapping(value = "/preReduceStock", method = RequestMethod.POST, headers = "Accept=application/json")
  public Integer preReduceStock(Integer purchaseNum, Integer productId) {
//    return service.preReduceStockTXC(purchaseNum, productId);

    return service.preReduceStockLCN(purchaseNum, productId);

//    return service.preReduceStockTCC(purchaseNum, productId);
  }
}
