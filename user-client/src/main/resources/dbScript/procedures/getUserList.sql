DELIMITER $$

DROP PROCEDURE IF EXISTS `getUserList` $$
CREATE PROCEDURE `getUserList`(
  IN p_start INT,
  IN p_size INT
)
begin
  DECLARE totalCount INT DEFAULT 0;
  set totalCount = (select count(*) from user_login);
  SELECT
	id,
	email,
	user_name as userName,
	password,
	status,
	totalCount
  FROM user_login
  limit p_start, p_size;
END $$
DELIMITER ;