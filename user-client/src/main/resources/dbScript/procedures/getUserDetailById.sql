DELIMITER $$

DROP PROCEDURE IF EXISTS `getUserDetailById` $$
CREATE PROCEDURE `getUserDetailById` (
	IN p_id INT
)
BEGIN
	SELECT
    id,
    email,
    user_name as userName,
    password,
    status
	FROM user_login
	WHERE id = p_id;
END $$
DELIMITER ;
