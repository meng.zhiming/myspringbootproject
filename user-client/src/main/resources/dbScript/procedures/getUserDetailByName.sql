DELIMITER $$

DROP PROCEDURE IF EXISTS `getUserDetailByName` $$
CREATE PROCEDURE `getUserDetailByName` (
	IN p_name varchar(255)
)
BEGIN
	SELECT * from user_login WHERE user_name = p_name;
END $$
DELIMITER ;
