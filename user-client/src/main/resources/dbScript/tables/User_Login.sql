drop table if exists user_login;

create table user_login (
  id int not null primary key auto_increment,
  email varchar(255),
  user_name varchar(255),
  password varchar(255),
  status bit
) engine=InnoDB;