package org.mzm.sc.userclient.dcl_app.composer;

import org.mzm.sc.userclient.dcl_app.request.ActionRequest;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;

public interface ActionComposer<T, R extends ActionRequest> {
    R compose(T request, ActionResponse ar);
}
