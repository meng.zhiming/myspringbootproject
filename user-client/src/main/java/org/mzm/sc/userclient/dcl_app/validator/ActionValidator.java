package org.mzm.sc.userclient.dcl_app.validator;

import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;

public interface ActionValidator<T> {
    ActionResponse validator(T request);
}
