package org.mzm.sc.userclient.activestream_loans.intf.model;

public interface IUser {

    Integer getId();

    void setId(Integer id);

    String getEmail();

    void setEmail(String email);

    String getUserName();

    void setUserName(String userName);

    String getPassword();

    void setPassword(String password);

    boolean isStatus();

    void setStatus(boolean status);
}
