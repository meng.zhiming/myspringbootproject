package org.mzm.sc.userclient.activestream_loans.impl.model;

import org.mzm.sc.userclient.activestream_loans.intf.model.IUser;

public class User implements IUser {

    private Integer id;
    private String email;
    private String userName;
    private String password;
    private boolean status = true;
//    private DepartmentEnum department;


    public User() {
    }

    public User(Integer id, String email, String userName, String password, boolean isSupport,
        boolean status) {
        this.id = id;
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.status = status;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isStatus() {
        return status;
    }

    @Override
    public void setStatus(boolean status) {
        this.status = status;
    }
}
