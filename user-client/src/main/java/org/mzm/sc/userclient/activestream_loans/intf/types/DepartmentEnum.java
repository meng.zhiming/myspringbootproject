package org.mzm.sc.userclient.activestream_loans.intf.types;

public enum DepartmentEnum {
    SYSTEMINMOTION("System-in-motion"), TOURIST("Tourist"), SUPPORTADMIN(
            "Super-Admin");

    private String name;

    DepartmentEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static DepartmentEnum fromString(String displayName) {
        if (displayName != null) {
            for (DepartmentEnum sectionEnum : DepartmentEnum.values()) {
                if (displayName.equalsIgnoreCase(sectionEnum.getName())) {
                    return sectionEnum;
                }
            }
        }
        return null;
    }
}
