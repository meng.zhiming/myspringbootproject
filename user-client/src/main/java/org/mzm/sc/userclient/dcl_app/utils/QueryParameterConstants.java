package org.mzm.sc.userclient.dcl_app.utils;

public class QueryParameterConstants {

    public static final String ID = "p_id";
    public static final String NAME = "p_name";

    // page
    public static final String START = "p_start";
    public static final String SIZE = "p_size";
}
