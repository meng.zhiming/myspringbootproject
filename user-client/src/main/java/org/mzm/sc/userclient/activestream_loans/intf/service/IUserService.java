package org.mzm.sc.userclient.activestream_loans.intf.service;

import org.mzm.sc.userclient.activestream_loans.intf.model.IUser;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;

public interface IUserService {

    ActionResponse saveUser(IUser user) throws Exception;

    ActionResponse deleteUser(Integer userId) throws Exception;
}
