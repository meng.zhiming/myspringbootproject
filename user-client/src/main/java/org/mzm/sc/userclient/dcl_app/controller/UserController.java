package org.mzm.sc.userclient.dcl_app.controller;

import org.mzm.sc.userclient.dcl_app.impl.UserDefinitionService;
import org.mzm.sc.userclient.dcl_app.model.PaginatedModel;
import org.mzm.sc.userclient.dcl_app.model.UserDetail;
import org.mzm.sc.userclient.dcl_app.uiInput.PaginatedInput;
import org.mzm.sc.userclient.dcl_app.uiInput.UserForUI;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    @Autowired
    private UserDefinitionService userService;

    @RequestMapping(value = "/getUserDetailById", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public UserDetail getUserDetailById(@RequestParam(value = "id", required = true) Integer id) {
        return userService.getUserDetailById(id);
    }

    //    @RequestMapping(value = "/getUserDetailByName", method = RequestMethod.GET)
    @Deprecated
    public UserDetail getUser(@RequestParam(value = "name", required = true) String name) {
        return userService.getUserDetailByName(name);
    }

    @RequestMapping(value = "/getUserList", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public PaginatedModel<UserDetail> getUserList(PaginatedInput input) {
        return userService.getUserList(input);
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ActionResponse saveUser(UserForUI userInput) {
        return userService.saveUser(userInput);
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ActionResponse deleteUser(UserForUI userInput) {
        return userService.deleteUser(userInput);
    }

    /*
    * For Feign
     */
    @RequestMapping(value = "getUserNameById", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public String getUserNameById(@RequestParam(value = "id", required = true) Integer id) {
        return userService.getUserNameById(id);
    }
}
