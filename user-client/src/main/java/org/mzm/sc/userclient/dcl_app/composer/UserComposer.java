package org.mzm.sc.userclient.dcl_app.composer;

import org.mzm.sc.userclient.activestream_loans.impl.model.User;
import org.mzm.sc.userclient.dcl_app.request.UserRequest;
import org.mzm.sc.userclient.dcl_app.uiInput.UserForUI;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;

public class UserComposer implements ActionComposer<UserForUI, UserRequest> {

    @Override
    public UserRequest compose(UserForUI request, ActionResponse ar) {

        UserRequest userRequest = new UserRequest();
        User user = new User();

        userRequest.setUser(user);
        userRequest.setAction(request.getAction());

        user.setId(request.getId());
        user.setUserName(request.getUserName());
        user.setPassword(request.getPassword());
        user.setEmail(request.getEmail());

        return userRequest;
    }
}
