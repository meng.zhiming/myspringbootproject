package org.mzm.sc.userclient.activestream_loans.impl.dao;


import org.mzm.sc.userclient.activestream_loans.intf.model.IUser;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    void saveOrUpdateUser(IUser user);

    boolean isSupport(Integer id);

    void deleteUser(Integer id);
}
