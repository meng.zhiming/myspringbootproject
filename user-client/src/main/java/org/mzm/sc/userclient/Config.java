package org.mzm.sc.userclient;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.mybatis.spring.annotation.MapperScan;

@Configuration
@ImportResource("classpath:springModule.xml")
@EnableTransactionManagement
@MapperScan("org.mzm.sc.userclient.activestream_loans.impl.dao")
public class Config {

}
