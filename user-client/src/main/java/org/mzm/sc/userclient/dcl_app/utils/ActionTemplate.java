package org.mzm.sc.userclient.dcl_app.utils;

import org.mzm.sc.userclient.dcl_app.composer.ActionComposer;
import org.mzm.sc.userclient.dcl_app.executor.ActionExecutor;
import org.mzm.sc.userclient.dcl_app.request.ActionRequest;
import org.mzm.sc.userclient.dcl_app.validator.ActionValidator;

public class ActionTemplate<T, R extends ActionRequest> {

    private ActionValidator<T> defaultValidator;
    private ActionComposer<T, R> defaultComposer;
    private ActionExecutor<R> defaultExecutor;

    public ActionResponse execute(T request) {
        ActionResponse ar = new ActionResponse();

        // 1. Validator
        if (defaultValidator != null) {
            ar = defaultValidator.validator(request);
            if (ar.hasError()) {
                return ar;
            }
        }

        // 2. Composer
        R actionRequest = defaultComposer.compose(request, ar);
        if (ar.hasError()) {
            return ar;
        }

        // 3. Executor
        ar = defaultExecutor.execute(actionRequest);

        return ar;
    }

    public ActionValidator getDefaultValidator() {
        return defaultValidator;
    }

    public void setDefaultValidator(ActionValidator defaultValidator) {
        this.defaultValidator = defaultValidator;
    }

    public ActionComposer getDefaultComposer() {
        return defaultComposer;
    }

    public void setDefaultComposer(ActionComposer defaultComposer) {
        this.defaultComposer = defaultComposer;
    }

    public ActionExecutor getDefaultExecutor() {
        return defaultExecutor;
    }

    public void setDefaultExecutor(ActionExecutor defaultExecutor) {
        this.defaultExecutor = defaultExecutor;
    }
}
