package org.mzm.sc.userclient.dcl_app.validator;

import org.mzm.sc.userclient.dcl_app.uiInput.UserForUI;
import org.mzm.sc.userclient.dcl_app.utils.ActionEnum;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserValidator implements ActionValidator<UserForUI> {

    Logger logger = LoggerFactory.getLogger(UserValidator.class);

    @Override
    public ActionResponse validator(UserForUI request) {

        ActionResponse ar = new ActionResponse();

        if (request == null) {
            logger.error("Request can not be empty");
            ar.addErrorResponse("Request can not be empty");
            return ar;
        }

        if (ActionEnum.REMOVE.equals(request.getAction()) || ActionEnum.UPDATE
            .equals(request.getAction())) {
            if (request.getId() == null) {
                logger.error("User Id can not be empty");
                ar.addErrorResponse("User Id can not be empty");
                return ar;
            }
        }

        if (ActionEnum.ADD.equals(request.getAction()) || ActionEnum.UPDATE
            .equals(request.getAction())) {
            if (request.getEmail() == null) {
                logger.error("User email can not be empty");
                ar.addErrorResponse("User email can not be empty");
                return ar;
            }
        }

        // TODO: more check for detail

        return ar;
    }
}
