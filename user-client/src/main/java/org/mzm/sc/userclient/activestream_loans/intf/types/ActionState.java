package org.mzm.sc.userclient.activestream_loans.intf.types;

public enum ActionState {
    /**
     * ActionState SUCCESS
     **/
    SUCCESS,

    /**
     * ActionState FAIL
     **/
    FAIL,
    /**
     * ActionState ERROR
     **/
    ERROR
}
