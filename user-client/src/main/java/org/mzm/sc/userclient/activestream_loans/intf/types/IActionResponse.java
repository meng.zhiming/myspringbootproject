package org.mzm.sc.userclient.activestream_loans.intf.types;

import java.util.List;

public interface IActionResponse {

    ActionState getActionState();

    void setActionState(ActionState actionState);

    void addErrorResponse(String errorMessage);

    boolean hasError();

    List<String> getResponseMessages();

    void setResponseMessages(List<String> responseMessages);
}
