package org.mzm.sc.userclient.activestream_loans.impl.utils;

public class ServiceException extends Exception{

    private static final long serialVersionUID = 1068059674113835115L;

    public ServiceException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
