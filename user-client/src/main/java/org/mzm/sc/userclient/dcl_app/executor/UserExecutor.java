package org.mzm.sc.userclient.dcl_app.executor;

import org.mzm.sc.userclient.activestream_loans.impl.utils.ServiceException;
import org.mzm.sc.userclient.activestream_loans.intf.service.IUserService;
import org.mzm.sc.userclient.activestream_loans.intf.types.ActionState;
import org.mzm.sc.userclient.dcl_app.request.UserRequest;
import org.mzm.sc.userclient.dcl_app.utils.ActionEnum;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class UserExecutor implements ActionExecutor<UserRequest> {

    private final static Logger logger = LoggerFactory.getLogger(UserExecutor.class);

    private IUserService userService;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public ActionResponse execute(UserRequest request) {

        ActionResponse ar = new ActionResponse();

        try {
            if (ActionEnum.ADD.equals(request.getAction()) || ActionEnum.UPDATE
                .equals(request.getAction())) {
                ar = userService.saveUser(request.getUser());
            } else if (ActionEnum.REMOVE.equals(request.getAction())) {
                ar = userService.deleteUser(request.getUser().getId());
            }

            if (!ar.hasError()) {
                ar.addSuccessResponse("User execute successful");
            }
        } catch (ServiceException e) {
            logger.error("Service Error in executor: " + e.getMessage());
            ar.addErrorResponse("Service Error: " + e.getMessage());
        } catch (Exception e) {
            logger.error("Error in executor: " + e.getMessage());
            ar.addErrorResponse("UnExcept Error: " + e.getMessage());
        }

        return ar;
    }
}
