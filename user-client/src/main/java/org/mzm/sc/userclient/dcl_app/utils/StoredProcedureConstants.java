package org.mzm.sc.userclient.dcl_app.utils;

public class StoredProcedureConstants {

    public static final String GET_USER_DETAIL_BY_ID = "getUserDetailById";
    public static final String GET_USER_DETAIL_BY_NAME = "getUserDetailByName";

    public static final String GET_USER_LIST = "getUserList";

}
