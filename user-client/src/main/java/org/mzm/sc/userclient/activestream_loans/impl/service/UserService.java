package org.mzm.sc.userclient.activestream_loans.impl.service;

import org.mzm.sc.userclient.activestream_loans.impl.dao.UserMapper;
import org.mzm.sc.userclient.activestream_loans.impl.utils.ServiceException;
import org.mzm.sc.userclient.activestream_loans.intf.model.IUser;
import org.mzm.sc.userclient.activestream_loans.intf.service.IUserService;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService implements IUserService {

    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserMapper userMapper;

    @Autowired
    private RedisService redisService;

    private static final String REDIS_USER_KEY = "user";

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ActionResponse saveUser(IUser user) throws ServiceException {
        ActionResponse ar = new ActionResponse();
        try {
            if (user.getId() == null) {
                logger.info("Add User");
            } else {
                logger.info(String.format("Update User for id %s", user.getId()));

                // User Updated, remove from redis
                redisService.delete(REDIS_USER_KEY + user.getId());
            }

            userMapper.saveOrUpdateUser(user);
            return ar;
        } catch (Exception e) {
            logger.error("Error when saving User! " + e.getMessage());
            throw new ServiceException("Error when saving User!");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ActionResponse deleteUser(Integer userId) throws ServiceException {
        ActionResponse ar = new ActionResponse();
        try {
            if (userId == null) {
                ar.addErrorResponse("User Id is null");
                return ar;
            }

            boolean isSupport = userMapper.isSupport(userId);
            if (isSupport) {
                ar.addErrorResponse("Support User can only be removed in DB");
                return ar;
            }

            userMapper.deleteUser(userId);
            return ar;
        } catch (Exception e) {
            logger.error("Error when deleting User! " + e.getMessage());
            throw new ServiceException("Error when deleting User!");
        }
    }
}
