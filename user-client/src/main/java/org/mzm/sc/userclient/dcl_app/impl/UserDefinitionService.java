package org.mzm.sc.userclient.dcl_app.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mzm.sc.userclient.dcl_app.model.PaginatedModel;
import org.mzm.sc.userclient.dcl_app.model.UserDetail;
import org.mzm.sc.userclient.dcl_app.request.UserRequest;
import org.mzm.sc.userclient.dcl_app.uiInput.PaginatedInput;
import org.mzm.sc.userclient.dcl_app.uiInput.UserForUI;
import org.mzm.sc.userclient.dcl_app.utils.ActionEnum;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;
import org.mzm.sc.userclient.dcl_app.utils.ActionTemplate;
import org.mzm.sc.userclient.dcl_app.utils.MyJdbcTemplate;
import org.mzm.sc.userclient.dcl_app.utils.QueryParameterConstants;
import org.mzm.sc.userclient.dcl_app.utils.StoredProcedureConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

public class UserDefinitionService {

    private final static Logger logger = LoggerFactory
        .getLogger(UserDefinitionService.class);

    private MyJdbcTemplate jdbcTemplate;

    private ActionTemplate<UserForUI, UserRequest> saveUserActionTemplate;
    private ActionTemplate<UserForUI, UserRequest> deleteUserActionTemplate;

    public ActionTemplate<UserForUI, UserRequest> getSaveUserActionTemplate() {
        return saveUserActionTemplate;
    }

    public void setDeleteUserActionTemplate(
        ActionTemplate<UserForUI, UserRequest> deleteUserActionTemplate) {
        this.deleteUserActionTemplate = deleteUserActionTemplate;
    }

    public void setSaveUserActionTemplate(
        ActionTemplate<UserForUI, UserRequest> saveUserActionTemplate) {
        this.saveUserActionTemplate = saveUserActionTemplate;
    }

    @Autowired
    public void setJdbcTemplate(MyJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public UserDetail getUserDetailById(Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put(QueryParameterConstants.ID, id);
        UserDetail user = jdbcTemplate
            .queryForObject(StoredProcedureConstants.GET_USER_DETAIL_BY_ID, UserDetail.class,
                params);
        return user;
    }

    public String getUserNameById(Integer id) {
        UserDetail userDetail = this.getUserDetailById(id);
        return userDetail == null ? null : userDetail.getUserName();
    }

    @Deprecated
    public UserDetail getUserDetailByName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put(QueryParameterConstants.NAME, name);
        UserDetail user = jdbcTemplate
            .queryForObject(StoredProcedureConstants.GET_USER_DETAIL_BY_NAME, UserDetail.class,
                params);
        return user;
    }

    public PaginatedModel<UserDetail> getUserList(PaginatedInput input) {
        PaginatedModel<UserDetail> page = jdbcTemplate
            .queryForPage(StoredProcedureConstants.GET_USER_LIST,
                new BeanPropertyRowMapper<>(UserDetail.class), input);

        return page;
    }

    public ActionResponse saveUser(UserForUI userInput) {
        logger.info("Saving User");
        userInput.setAction(ActionEnum.UPDATE);
        ActionResponse ar = saveUserActionTemplate.execute(userInput);

        return ar;
    }

    public ActionResponse deleteUser(UserForUI userInput) {
        logger.info("Deleting User");
        userInput.setAction(ActionEnum.REMOVE);
        ActionResponse ar = deleteUserActionTemplate.execute(userInput);

        return ar;
    }
}
