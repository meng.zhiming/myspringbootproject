package org.mzm.sc.userclient.dcl_app.model;

public interface PaginatedTotalCount {
    Integer getTotalCount();

    void setTotalCount(Integer totalCount);
}
