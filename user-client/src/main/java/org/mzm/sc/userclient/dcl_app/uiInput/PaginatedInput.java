package org.mzm.sc.userclient.dcl_app.uiInput;

public class PaginatedInput {
    private Integer start;
    private Integer size;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
