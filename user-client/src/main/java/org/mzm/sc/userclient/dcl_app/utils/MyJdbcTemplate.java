package org.mzm.sc.userclient.dcl_app.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mzm.sc.userclient.dcl_app.model.PaginatedModel;
import org.mzm.sc.userclient.dcl_app.model.PaginatedTotalCount;
import org.mzm.sc.userclient.dcl_app.uiInput.PaginatedInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

@Component
public class MyJdbcTemplate {

    private final static Logger logger = LoggerFactory
        .getLogger(MyJdbcTemplate.class);

    private static final String RESULTSERTNAME = "p_resultset";
    private static final String UPDATED_ROW_COUNT = "p_rowcount";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public <T> T queryForObject(String sql, Class<T> obj) {
        return this.queryForObject(sql, obj, new HashMap<>());
    }

    public <T> T queryForObject(String sql, Class<T> obj,
        Map<String, Object> params) {

        List<T> result = this
            .query(sql, new BeanPropertyRowMapper<>(obj), params);

        if (result != null && !result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    public <T> List<T> query(String procedureName, RowMapper<T> rm) {
        return this.query(procedureName, rm, new HashMap<>());
    }

    public <T> List<T> query(String procedureName, RowMapper<T> rm,
        Map<String, Object> params) {
        List<SqlParameter> procedureParams = new ArrayList<>();
        if (rm == null) {
            procedureParams.add(0, new SqlOutParameter(UPDATED_ROW_COUNT, 4));
        }

        SimpleJdbcCall procedure = new SimpleJdbcCall(jdbcTemplate)
            .withCatalogName(null)
            .withProcedureName(procedureName);

//    procedure.withoutProcedureColumnMetaDataAccess();

        StringBuilder sb = new StringBuilder();
        sb.append("Params: ");
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                sb.append(entry.getKey()).append(" = ").append(entry.getValue())
                    .append(" -- ");
            }
        }

        procedure.returningResultSet(RESULTSERTNAME, rm);
        procedure.compile();

        if (params != null && !params.isEmpty()) {
            logger.info(sb.toString());
        }

        Map<String, Object> result = procedure.execute(params);
        List<T> list = (List<T>) result.get(RESULTSERTNAME);

        return list;
    }

    public <T extends PaginatedTotalCount> PaginatedModel<T> queryForPage(String procedureName,
        RowMapper<T> rm, PaginatedInput input) {
        return queryForPage(procedureName, rm, new HashMap<>(), input);
    }

    public <T extends PaginatedTotalCount> PaginatedModel<T> queryForPage(String procedureName,
        RowMapper<T> rm, Map<String, Object> params, PaginatedInput input) {
        if (input == null || input.getStart() == null || input.getSize() == null) {
            return PaginatedModel.EMPTY_MODEL;
        }

        PaginatedModel<T> pageModel = new PaginatedModel<>();

        Integer start = input.getStart();
        Integer size = input.getSize();

        if (start <= 0 || size <= 0) {
            start = PaginatedModel.DEFAULT_START;
            size = PaginatedModel.DEFAULT_SIZE;
        }

        pageModel.setStart(start);
        pageModel.setSize(size);

        params.put(QueryParameterConstants.START, start - 1);
        params.put(QueryParameterConstants.SIZE, size);

        List<T> list = this.query(procedureName, rm, params);

        if (list != null && !list.isEmpty()) {
            pageModel.setTotalCount(list.get(0).getTotalCount());
        }

        pageModel.setList(list);

        return pageModel;
    }
}
