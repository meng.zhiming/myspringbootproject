package org.mzm.sc.userclient.dcl_app.uiInput;

import org.mzm.sc.userclient.dcl_app.utils.ActionEnum;

public class UserForUI {

    private Integer id;
    private String email;
    private String userName;
    private String password;

    private ActionEnum action;

    public UserForUI() {
    }

    public UserForUI(Integer id, String email, String userName, String password) {
        this.id = id;
        this.email = email;
        this.userName = userName;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ActionEnum getAction() {
        return action;
    }

    public void setAction(ActionEnum action) {
        this.action = action;
    }
}
