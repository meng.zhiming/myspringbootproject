package org.mzm.sc.userclient.dcl_app.executor;

import org.mzm.sc.userclient.dcl_app.request.ActionRequest;
import org.mzm.sc.userclient.dcl_app.utils.ActionResponse;

public interface ActionExecutor<R extends ActionRequest> {
    ActionResponse execute(R request);
}
