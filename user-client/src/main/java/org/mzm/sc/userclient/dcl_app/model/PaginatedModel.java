package org.mzm.sc.userclient.dcl_app.model;

import java.util.List;

public class PaginatedModel<T> {

    private Integer totalCount;
    private Integer start;
    private Integer size;

    private List<T> list;

    public static final PaginatedModel EMPTY_MODEL = new PaginatedModel();
    public static final Integer DEFAULT_START = 1;
    public static final Integer DEFAULT_SIZE = 10;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
