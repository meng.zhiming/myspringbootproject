package org.mzm.sc.userclient.dcl_app.request;

import org.mzm.sc.userclient.activestream_loans.impl.model.User;
import org.mzm.sc.userclient.dcl_app.utils.ActionEnum;

public class UserRequest implements ActionRequest {

    private User user;
    private ActionEnum action;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ActionEnum getAction() {
        return action;
    }

    public void setAction(ActionEnum action) {
        this.action = action;
    }
}
