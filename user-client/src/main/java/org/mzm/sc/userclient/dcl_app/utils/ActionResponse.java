package org.mzm.sc.userclient.dcl_app.utils;

import java.util.ArrayList;
import java.util.List;
import org.mzm.sc.userclient.activestream_loans.intf.types.ActionState;
import org.mzm.sc.userclient.activestream_loans.intf.types.IActionResponse;

public class ActionResponse implements IActionResponse {

    private ActionState actionState;
    private List<String> responseMessages = new ArrayList<>();

    public ActionState getActionState() {
        return actionState;
    }

    public void setActionState(ActionState actionState) {
        this.actionState = actionState;
    }

    public void addErrorResponse(String errorMessage) {
        responseMessages.add(errorMessage);
        actionState = ActionState.ERROR;
    }

    public void addSuccessResponse(String successMessage) {
        responseMessages.add(successMessage);
        actionState = ActionState.SUCCESS;
    }

    public boolean hasError() {
        return !responseMessages.isEmpty() && ActionState.ERROR.equals(actionState);
    }

    public List<String> getResponseMessages() {
        return responseMessages;
    }

    public void setResponseMessages(List<String> responseMessages) {
        this.responseMessages = responseMessages;
    }
}
