package org.mzm.sc.questionclient.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import org.mzm.sc.questionclient.model.Answer;
import org.mzm.sc.questionclient.model.Question;
import org.mzm.sc.questionclient.model.QuestionForList;
import org.mzm.sc.questionclient.service.QuestionService;
import org.mzm.sc.questionclient.utils.ActionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/question")
public class QuestionController {

    @Autowired
    private QuestionService service;


    @RequestMapping(value = "/getQuestionList")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public IPage<QuestionForList> getQuestionList() {
        return service.getQuestionList();
    }

    @RequestMapping(value = "/getQuestionDetailById/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Question getQuestionDetailById(@PathVariable(name = "id", required = true) Integer id) {
        return service.getQuestionDetailById(id);
    }

    @RequestMapping(value = "/saveOrUpdateQuestion", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ActionResponse saveOrUpdateQuestion(Question question,
        @RequestParam(value = "action", required = true) String action) {
        return service.saveOrUpdateQuestion(question, action);
    }

    @RequestMapping(value = "/answerQuestion", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ActionResponse answerQuestion(Answer answer) {
        return service.answerQuestion(answer);
    }
}
