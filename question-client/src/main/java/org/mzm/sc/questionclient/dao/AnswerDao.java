package org.mzm.sc.questionclient.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import javax.annotation.Resource;
import org.mzm.sc.questionclient.mapper.AnswerMapper;
import org.mzm.sc.questionclient.model.Answer;
import org.springframework.stereotype.Component;

@Component
public class AnswerDao {

    @Resource
    private AnswerMapper answerMapper;

    public Answer getAnswerInDb(Answer answer) {
        return answerMapper.selectOne(getKeyWrapper(answer));
    }

    public void insertAnswer(Answer answer) {
        answerMapper.insert(answer);
    }

    public void updateAnswer(Answer answer) {
        answerMapper.update(answer, getKeyWrapper(answer));
    }

    private QueryWrapper<Answer> getKeyWrapper(Answer answer) {
        return new QueryWrapper<Answer>()
            .eq("question_id", answer.getQuestionId())
            .eq("user_id", answer.getUserId());
    }


}
