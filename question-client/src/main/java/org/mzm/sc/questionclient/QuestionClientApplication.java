package org.mzm.sc.questionclient;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("org.mzm.sc.questionclient.mapper")
@EnableFeignClients
public class QuestionClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuestionClientApplication.class, args);
    }

}
