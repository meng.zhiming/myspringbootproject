package org.mzm.sc.questionclient.service;

import org.mzm.sc.questionclient.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-client", configuration = FeignConfig.class)
@RequestMapping(value = "/api/user")
public interface FeignUserService {

    @RequestMapping(value = "getUserNameById", method = RequestMethod.GET)
    String getUserNameById(@RequestParam(value = "id", required = true) Integer id);
}
