package org.mzm.sc.questionclient.utils;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ActionResponse {

    public enum ActionState {
        /**
         * ActionState SUCCESS
         **/
        SUCCESS,

        /**
         * ActionState FAIL
         **/
        FAIL,
        /**
         * ActionState ERROR
         **/
        ERROR
    }

    private ActionState actionState = ActionState.SUCCESS;
    private List<String> errorList = new ArrayList<>();
    private String successMessage;

    public void addResponse(String errorMessage) {
        errorList.add(errorMessage);
        actionState = ActionState.ERROR;
    }
}
