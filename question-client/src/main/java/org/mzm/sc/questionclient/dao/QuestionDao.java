package org.mzm.sc.questionclient.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;
import javax.annotation.Resource;
import org.mzm.sc.questionclient.mapper.QuestionMapper;
import org.mzm.sc.questionclient.model.Question;
import org.springframework.stereotype.Component;

@Component
public class QuestionDao {

    @Resource
    private QuestionMapper questionMapper;

    public IPage<Question> getQuestions() {

        Integer pageNo = Integer.valueOf(1);
        Integer pageSize = Integer.valueOf(10);
        IPage<Question> page = new Page<>(pageNo, pageSize);

        QueryWrapper<Question> wrapper = new QueryWrapper<>();
        return questionMapper.selectPage(page, wrapper);
    }

    public Question getQuestionById(Integer id) {
        return questionMapper.selectById(id);
    }

    public void insertQuestion(Question question) {
        questionMapper.insert(question);
    }


    public void updateQuestionById(Question question) {
        questionMapper.updateById(question);
    }
}
