package org.mzm.sc.questionclient.model;

import com.baomidou.mybatisplus.annotation.TableId;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionForList {
    private Integer questionId;
    private String questionType;
    private String questionTitle;
    private Date createdDate = new Date();
    private String createdByName;
}
