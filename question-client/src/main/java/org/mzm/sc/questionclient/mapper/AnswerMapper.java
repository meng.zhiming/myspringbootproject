package org.mzm.sc.questionclient.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.mzm.sc.questionclient.model.Answer;
import org.springframework.stereotype.Repository;

public interface AnswerMapper extends BaseMapper<Answer> {

}
