package org.mzm.sc.questionclient.service;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisService {

    private static final Logger logger = LoggerFactory.getLogger(RedisService.class.getName());

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    public void set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
        } catch (Exception e) {
            logger.error(String.format("Redis set fail because: %s", e.getMessage()));
        }
    }

    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }
}
