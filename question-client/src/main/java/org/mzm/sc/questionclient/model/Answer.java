package org.mzm.sc.questionclient.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@TableName(value = "User_Answer")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Answer {

    private Integer userId;
    private Integer questionId;
    private String userAnswerContent;
    private Date userAnswerDate = new Date();
    private boolean status;
}
