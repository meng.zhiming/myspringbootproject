package org.mzm.sc.questionclient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
//@EnableWebMvc
@ComponentScan(value = "org.mzm.sc.questionclient.controller")
public class SwaggerConfig {

  @Bean
  public Docket customDocket() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    Contact contact = new Contact("mzm", "http://www.mzm.org/", "916354835@qq.com");
    return new ApiInfoBuilder()
        .title("API Interface")
        .description("API Interface")
        .contact(contact)
        .version("10")
        .build();
  }
}
