package org.mzm.sc.questionclient.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@TableName("Question")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Question {

    @TableId
    private Integer questionId;
    private String questionType;
    private String questionTitle;
    private String questionContent;
    private String standardAnswerContent;
    private Integer createdBy;
    private Date createdDate = new Date();

    @TableField(exist = false)
    private String createdByName;

    public QuestionForList convert() {
        QuestionForList questionForList = new QuestionForList(questionId, questionType,
            questionTitle, createdDate, createdByName);
        return questionForList;
    }
}
