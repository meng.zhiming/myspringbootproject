package org.mzm.sc.questionclient.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;
import java.util.stream.Collectors;
import org.mzm.sc.questionclient.dao.AnswerDao;
import org.mzm.sc.questionclient.dao.QuestionDao;
import org.mzm.sc.questionclient.model.Answer;
import org.mzm.sc.questionclient.model.Question;
import org.mzm.sc.questionclient.model.QuestionForList;
import org.mzm.sc.questionclient.utils.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class QuestionService {

    private static final Logger logger = LoggerFactory.getLogger(QuestionService.class.getName());

    private static final String ADD = "add";
    private static final String UPDATE = "update";
    private static final String REMOVE = "remove";
    private static final String REDIS_USER_KEY = "user";

    @Autowired
    private QuestionDao questionDao;
    @Autowired
    private AnswerDao answerDao;
    @Autowired
    private RedisService redisService;
    @Autowired
    private FeignUserService feignUserService;

    public IPage<QuestionForList> getQuestionList() {
        IPage<Question> questions = questionDao.getQuestions();

        List<QuestionForList> questionForLists = questions.getRecords().stream().map(q -> {
            getAndFillUserName(q);
            QuestionForList question = q.convert();
            return question;
        }).collect(Collectors.toList());

        IPage<QuestionForList> questionForList = new Page<>(questions.getCurrent(),
            questions.getSize(), questions.getTotal());
        questionForList.setPages(questions.getPages());
        questionForList.setRecords(questionForLists);

        return questionForList;
    }

    public Question getQuestionDetailById(Integer id) {
        Question question = questionDao.getQuestionById(id);

        if (question != null) {
            getAndFillUserName(question);
        }

        return question;
    }

    // Add or Update the Question Detail.
    public ActionResponse saveOrUpdateQuestion(Question question, String action) {
        ActionResponse ar = new ActionResponse();
        if (question == null) {
            ar.addResponse("Question can not be null");
            return ar;
        }

        if (!StringUtils.hasText(action)) {
            ar.addResponse("action can not be null");
            return ar;
        }

        if (question.getQuestionId() == null && !action.equalsIgnoreCase(ADD)) {
            ar.addResponse("Invalid request, missing Question Id");
            return ar;
        }

        try {
            if (action.equalsIgnoreCase(ADD)) {
                questionDao.insertQuestion(question);
            } else if (action.equalsIgnoreCase(UPDATE)) {
                questionDao.updateQuestionById(question);
            } else {
                ar.addResponse(String.format("Invalid action %s!" + action));
                return ar;
            }
        } catch (Exception e) {
            logger.error(String.format("Save or Update Question error %s", e.getMessage()));
            ar.addResponse(String.format("Save or Update Question error %s", e.getMessage()));
            return ar;
        }

        return ar;
    }

    // Answer the Question or update the Answer, need user-client detail.
    public ActionResponse answerQuestion(Answer answer) {
        ActionResponse ar = new ActionResponse();
        if (answer == null) {
            ar.addResponse("Answer can not be null");
            return ar;
        }

        if (answer.getUserId() == null) {
            ar.addResponse("Submit User can not be null");
            return ar;
        }

        if (answer.getQuestionId() == null) {
            ar.addResponse("Question Id can not be null");
            return ar;
        }

        if (StringUtils.isEmpty(answer.getUserAnswerContent())) {
            ar.addResponse("Answer Content can not be null");
            return ar;
        }

        Answer answerInDb = answerDao.getAnswerInDb(answer);

        if (answerInDb == null) {
            logger.info("User never answer the question before");
            answerDao.insertAnswer(answer);
        } else {
            logger.info("Update user's answer");
            answerDao.updateAnswer(answer);
        }

        return ar;
    }

    private void getAndFillUserName(Question question) {
        Object userName = redisService.get(REDIS_USER_KEY + question.getCreatedBy());
        logger.info(
            String.format("Get From Redis %s : %s", REDIS_USER_KEY + question.getCreatedBy(),
                userName == null ? null : userName));

        if (userName == null) {
            String userNameFromUser = feignUserService.getUserNameById(question.getCreatedBy());
            if (StringUtils.hasText(userNameFromUser)) {
                redisService.set(REDIS_USER_KEY + question.getCreatedBy(), userNameFromUser);
            }
            question.setCreatedByName(userNameFromUser);
        } else {
            question.setCreatedByName(userName.toString());
        }
    }
}
