package org.mzm.sc.questionclient.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.mzm.sc.questionclient.model.Question;
import org.springframework.stereotype.Repository;

public interface QuestionMapper extends BaseMapper<Question> {

}
