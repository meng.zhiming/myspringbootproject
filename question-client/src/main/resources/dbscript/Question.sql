drop table if exists Question;

Create table Question
(
Question_Id Int not null primary key auto_increment,
Question_Type Varchar(50),
Question_Title Varchar(255),
Question_Content longText,
Standard_Answer_Content longText,
Created_By Int,
Created_Date TimeStamp
) engine=InnoDB;

drop table if exists User_Answer;

Create table User_Answer
(
User_Id Int,
Question_Id Int,
User_Answer_Content longText,
User_Answer_Date TimeStamp,
Status Bit
) engine=InnoDB;