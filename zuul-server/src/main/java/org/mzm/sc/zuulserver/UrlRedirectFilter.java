package org.mzm.sc.zuulserver;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;


@Component
public class UrlRedirectFilter extends ZuulFilter {

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String url = request.getRequestURI();
        String[] split = url.split("/");
        if (split.length >= 2) {
            if ("user".equalsIgnoreCase(split[1])) {
                url = url.replace(split[1], "api/user");
                ctx.put(FilterConstants.REQUEST_URI_KEY, url);
            } else if ("question".equalsIgnoreCase(split[1])) {
                url = url.replace(split[1], "api/question");
                ctx.put(FilterConstants.REQUEST_URI_KEY, url);
            } else if ("product".equalsIgnoreCase(split[1])) {
                url = url.replace(split[1], "api/product");
                ctx.put(FilterConstants.REQUEST_URI_KEY, url);
            } else if ("warehouse".equalsIgnoreCase(split[1])) {
                url = url.replace(split[1], "api/warehouse");
                ctx.put(FilterConstants.REQUEST_URI_KEY, url);
            } else if ("order".equalsIgnoreCase(split[1])) {
                url = url.replace(split[1], "api/order");
                ctx.put(FilterConstants.REQUEST_URI_KEY, url);
            }
        }
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public String filterType() {
        return FilterConstants.ROUTE_TYPE;
    }

}
