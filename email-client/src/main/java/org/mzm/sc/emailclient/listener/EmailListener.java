package org.mzm.sc.emailclient.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmailListener {

    @RabbitListener(queues = "thanksQueue")
    @RabbitHandler
    public void receiverThanksEmail(String emailType) {
        log.info(String.format("receive thanks email: %s", emailType));
    }

    @RabbitListener(queues = "orderQueue")
    @RabbitHandler
    public void receiverOrderEmail(String emailType) {
        log.info(String.format("receive order email: %s", emailType));
    }
}
