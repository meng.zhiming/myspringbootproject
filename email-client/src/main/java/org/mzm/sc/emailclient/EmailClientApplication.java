package org.mzm.sc.emailclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EmailClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmailClientApplication.class, args);
    }

}
