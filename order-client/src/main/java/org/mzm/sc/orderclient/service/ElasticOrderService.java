package org.mzm.sc.orderclient.service;

import org.mzm.sc.orderclient.model.OrderDetail;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ElasticOrderService extends ElasticsearchRepository<OrderDetail, Integer> {

}
