package org.mzm.sc.orderclient.util;

public enum EmailTypeEnum {
  ORDER_SUCCESS_EMAIL("order success"), THANKS_FOR_PURCHASE_EMAIL("thanks for purchase");

  private String name;

  EmailTypeEnum(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public static EmailTypeEnum fromString(String displayName) {
    if (displayName != null) {
      for (EmailTypeEnum sectionEnum : EmailTypeEnum.values()) {
        if (displayName.equalsIgnoreCase(sectionEnum.getName())) {
          return sectionEnum;
        }
      }
    }
    return null;
  }
}
