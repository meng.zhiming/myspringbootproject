package org.mzm.sc.orderclient;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue orderQueue() {
        return new Queue("orderQueue");
    }

    @Bean
    public Queue thanksQueue() {
        return new Queue("thanksQueue");
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("topicExchange");
    }

    @Bean
    public Binding bindingOrderMQExchange(Queue orderQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(orderQueue).to(topicExchange).with("ose");
    }

    @Bean
    public Binding bindingPurchaseMQExchange(Queue thanksQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(thanksQueue).to(topicExchange).with("tfpe");
    }
}
