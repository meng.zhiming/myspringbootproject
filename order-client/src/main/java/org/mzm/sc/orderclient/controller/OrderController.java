package org.mzm.sc.orderclient.controller;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.mzm.sc.orderclient.model.OrderDetail;
import org.mzm.sc.orderclient.service.ElasticOrderService;
import org.mzm.sc.orderclient.service.OrderService;
import org.mzm.sc.orderclient.util.ActionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/order")
public class OrderController {

  @Autowired
  private OrderService service;

  @Autowired
  private ElasticOrderService elasticOrderService;

  @RequestMapping(value = "/purchaseOneProduct", method = RequestMethod.POST)
  public ActionResponse purchaseOneProduct(OrderDetail orderDetail) {
    ActionResponse ar = new ActionResponse();
    try {
      ar = service.purchaseOneProduct(orderDetail);
    } catch (Exception e) {
      ar.addErrorResponse("Internal Service Error " + e.getMessage());
    }
    return ar;
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @RequestMapping(value = "esOrder", method = RequestMethod.GET)
  public List<OrderDetail> searchOrder(String keyword) {
    QueryStringQueryBuilder builder = new QueryStringQueryBuilder(keyword);
    Iterable<OrderDetail> searchResult = elasticOrderService.search(builder);
    Iterator<OrderDetail> iterator = searchResult.iterator();
    return Lists.newArrayList(iterator);
  }
}
