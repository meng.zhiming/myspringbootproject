package org.mzm.sc.orderclient.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitMQService {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(String message, String topic) {
        log.info("Send email");
        log.info("amqpTemplate：" + amqpTemplate);
        amqpTemplate.convertAndSend("topicExchange", topic, message);
    }
}
