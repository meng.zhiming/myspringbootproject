package org.mzm.sc.orderclient.service;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.mzm.sc.orderclient.dao.OrderMapper;
import org.mzm.sc.orderclient.model.OrderDetail;
import org.mzm.sc.orderclient.util.ActionResponse;
import org.mzm.sc.orderclient.util.EmailTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderService {

  private Logger logger = LoggerFactory.getLogger(OrderService.class);

  @Autowired
  private OrderMapper orderDao;

  @Autowired
  private FeignWarehouseService feignWarehouseService;

  @Autowired
  private RabbitMQService rabbitMQService;

  @Autowired
  private ElasticOrderService elasticOrderService;

  @LcnTransaction
  public ActionResponse purchaseOneProduct(OrderDetail orderDetail) {
    // purchase one product
    ActionResponse ar = new ActionResponse();

    if (orderDetail == null) {
      ar.addErrorResponse("Order Detail can not be null");
      return ar;
    }

    if (orderDetail.getCustomerId() == null) {
      ar.addErrorResponse("CustomerId can not be empty");
      return ar;
    }

    if (orderDetail.getProductId() == null) {
      ar.addErrorResponse("productId can not be empty");
      return ar;
    }

    if (orderDetail.getProductCnt() <= 0) {
      ar.addErrorResponse("purchase number can not less than 1");
      return ar;
    }

    // 1. reduce stock from warehouse-client
    // TODO: need do hystrix
    Integer status = feignWarehouseService
        .preReduceStock(orderDetail.getProductCnt(), orderDetail.getProductId());

    logger.info(String.format("get status %d from warehouse-client", status));
    if (status == 0) {
      ar.addErrorResponse("No enough Stock for this product");
    }

    // 2. reduce success, create an order, add to es.
    boolean orderStatus = addNewOrder(orderDetail, ar);

    // 3. If all success, send Email
    if (orderStatus) {
      sendEmail(EmailTypeEnum.ORDER_SUCCESS_EMAIL, EmailTypeEnum.THANKS_FOR_PURCHASE_EMAIL);
    }

    return ar;
  }

  private void sendEmail(EmailTypeEnum... emailTypes) {
    for (EmailTypeEnum emailType : emailTypes) {
      if (EmailTypeEnum.ORDER_SUCCESS_EMAIL.equals(emailType)) {
        rabbitMQService.send(emailType.getName(), "ose");
      } else if (EmailTypeEnum.THANKS_FOR_PURCHASE_EMAIL.equals(emailType)) {
        rabbitMQService.send(emailType.getName(), "tfpe");
      }
    }
  }

  private boolean addNewOrder(OrderDetail orderDetail, ActionResponse ar) {
    int result = orderDao.addNewOrder(orderDetail);
    if (result == 1) {
      logger.info(String
          .format("order for %s add successfully, id: %d", orderDetail.getProductName(),
              orderDetail.getOrderId()));
    } else {
      logger.error(String.format("order for %d add fail", orderDetail.getProductId()));
      ar.addErrorResponse(String.format("order for %s add fail", orderDetail.getProductName()));
      return false;
    }

    int resultForDetail = orderDao.addNewOrderDetail(orderDetail);
    if (resultForDetail == 1) {
      logger
          .info(String.format("orderDetail for %s add successfully", orderDetail.getProductName()));
      ar.addSuccessResponse(
          String.format("orderDetail for %s add successfully", orderDetail.getProductName()));
    } else {
      logger.error(String.format("orderDetail for %d add fail", orderDetail.getProductId()));
      ar.addErrorResponse(
          String.format("orderDetail for %s add fail", orderDetail.getProductName()));
      return false;
    }

    // Add to es
    elasticOrderService.save(orderDetail);

    return true;
  }
}
