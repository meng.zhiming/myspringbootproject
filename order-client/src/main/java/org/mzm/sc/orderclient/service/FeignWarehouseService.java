package org.mzm.sc.orderclient.service;

import org.mzm.sc.orderclient.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "warehouse-client", configuration = FeignConfig.class)
@RequestMapping(value = "/api/warehouse")
public interface FeignWarehouseService {

    @RequestMapping(value = "/preReduceStock", method = RequestMethod.POST, headers = "Accept=application/json")
    Integer preReduceStock(@RequestParam("purchaseNum") Integer purchaseNum,
        @RequestParam("productId") Integer productId);
}
