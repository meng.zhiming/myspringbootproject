package org.mzm.sc.orderclient.model;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Document(indexName = "orders", type = "order", refreshInterval = "-1")
public class OrderDetail extends Order implements Serializable {

  private Integer orderId;
  private Integer productId;
  private String productName;
  private Integer productCnt;
  private BigDecimal productPrice;
  private BigDecimal averageCost;
  private Float weight;
  //优惠分摊金额
  private BigDecimal feeMoney;
  //仓库ID
  private Integer wId;
}
