package org.mzm.sc.orderclient;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(value = "org.mzm.sc.orderclient.dao")
@EnableFeignClients
@EnableDistributedTransaction
public class OrderClientApplication {

  public static void main(String[] args) {
    SpringApplication.run(OrderClientApplication.class, args);
  }
}
