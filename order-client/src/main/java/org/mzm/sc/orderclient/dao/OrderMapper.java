package org.mzm.sc.orderclient.dao;

import org.mzm.sc.orderclient.model.OrderDetail;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper {
  int addNewOrder(OrderDetail orderDetail);

  int addNewOrderDetail(OrderDetail orderDetail);
}
