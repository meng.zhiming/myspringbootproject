package org.mzm.sc.orderclient.model;

import java.math.BigDecimal;
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Order {
  private Integer id;
  // 订单编号
  private BigDecimal orderSn;
  // 下单人ID
  private Integer customerId;
  // 收货人姓名
  private String shippingUserName;
  private Integer province;
  private Integer city;
  private Integer district;
  private String address;
  //支付方式：1现金，2余额，3网银，4支付宝，5微信
  private Integer paymentMethod;
  //订单金额
  private BigDecimal orderMoney;
  //优惠金额
  private BigDecimal districtMoney;
  //运费金额
  private BigDecimal shippingMoney;
  //支付金额
  private BigDecimal paymentMoney;
  //快递公司名称
  private String shippingCompName;
  //快递单号
  private String shippingSn;
  //发货时间
  private Date shippingTime;
  //支付时间
  private Date payTime;
  //收货时间
  private Date receiveTime;
  //订单状态
  private Integer orderStatus;
  //订单积分
  private Integer orderPoint;
  //发票抬头
  private String invoiceTime;
}
