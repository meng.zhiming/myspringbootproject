package org.mzm.sc.orderclient;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.DiscardOldestPolicy;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mzm.sc.orderclient.model.OrderDetail;
import org.mzm.sc.orderclient.service.OrderService;
import org.mzm.sc.orderclient.util.ActionResponse;
import org.mzm.sc.orderclient.util.ActionResponse.ActionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderClientApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(OrderClientApplication.class);

    @Autowired
    private OrderService service;

    private RestTemplate restTemplate = new RestTemplate();

    private OrderDetail order;

    private ThreadPoolExecutor pool = new ThreadPoolExecutor(4, 6, 60, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(), new DiscardOldestPolicy());

//    @Before
//    public void init() {
//        order = new OrderDetail();
//        order.setOrderSn(new BigDecimal(123));
//        order.setCustomerId(1);
//        order.setShippingUserName("mzm");
//        order.setProvince(1);
//        order.setCity(1);
//        order.setDistrict(1);
//        order.setAddress("Address");
//        order.setPaymentMethod(1);
//        order.setOrderMoney(new BigDecimal(10000));
//        order.setDistrictMoney(new BigDecimal(50));
//        order.setShippingMoney(new BigDecimal(10));
//        order.setPaymentMoney(new BigDecimal(9960));
//        order.setShippingCompName("shunfeng");
//        order.setShippingSn("sf");
//        order.setShippingTime(new Date(System.currentTimeMillis()));
//        order.setPayTime(new Date(System.currentTimeMillis()));
//        order.setReceiveTime(new Date(System.currentTimeMillis()));
//        order.setOrderStatus(1);
//        order.setOrderPoint(20);
//        order.setInvoiceTime("fp");
//        order.setProductId(6);
//        order.setProductName("MAC");
//        order.setProductCnt(5);
//        order.setProductPrice(new BigDecimal(10000));
//        order.setAverageCost(new BigDecimal(6000));
//        order.setFeeMoney(new BigDecimal(0));
//        order.setWId(1);
//    }

    private void initMap(MultiValueMap<String, String> map) {
        map.add("orderSn", String.valueOf(123));
        map.add("customerId", String.valueOf(1));
        map.add("shippingUserName", "mzm");
        map.add("province", String.valueOf(1));
        map.add("city", String.valueOf(1));
        map.add("district", String.valueOf(1));
        map.add("address", "Address");
        map.add("paymentMethod", String.valueOf(1));
        map.add("orderMoney", String.valueOf(10000));
        map.add("districtMoney", String.valueOf(50));
        map.add("shippingMoney", String.valueOf(10));
        map.add("paymentMoney", String.valueOf(9960));
        map.add("shippingCompName", "shunfeng");
        map.add("shippingSn", "sf");
        map.add("shippingTime", String.valueOf(new Date(System.currentTimeMillis())));
        map.add("payTime", String.valueOf(new Date(System.currentTimeMillis())));
        map.add("receiveTime", String.valueOf(new Date(System.currentTimeMillis())));
        map.add("orderStatus", String.valueOf(1));
        map.add("orderPoint", String.valueOf(20));
        map.add("invoiceTime", "fp");
        map.add("productId", String.valueOf(6));
        map.add("productName", "MAC");
        map.add("productCnt", String.valueOf(6));
        map.add("productPrice", String.valueOf(10000));
        map.add("averageCost", String.valueOf(6000));
        map.add("feeMoney", String.valueOf(0));
        map.add("wId", String.valueOf(1));
    }

    @Test
    public void contextLoads() throws InterruptedException {
        Assert.assertNotNull(service);
        Assert.assertNotNull(pool);

        CountDownLatch count = new CountDownLatch(30);

        String url = "http://127.0.0.1:9999/order/purchaseOneProduct";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        initMap(map);

        // TODO: Should get from oauth-client, here just quick test
        headers.setBearerAuth("40fae693-74d1-4fb1-9ddf-e540d44080f8");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        restTemplate.postForEntity(url, request, ActionResponse.class);

        for (int i = 0; i <= 30; i++) {
            logger.info("purchase: " + i);
            pool.execute(new Thread(() -> {
                    logger.info("Call in");
                    ResponseEntity<ActionResponse> response = restTemplate
                        .exchange(url, HttpMethod.POST, request, ActionResponse.class);

                    count.countDown();
                    ActionResponse ar = response.getBody();
                    if (ActionState.ERROR.equals(ar.getActionState())) {
//                        count.countDown();
//                        Assert.fail("purchase fail " + ar.getResponseMessages());
                        logger.error("purchase fail: " + ar.getResponseMessages());
                    }
                })
            );
        }

        count.await();
        TimeUnit.SECONDS.sleep(0);
        pool.shutdownNow();
    }
}
