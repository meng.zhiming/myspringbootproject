package org.mzm.sc.oauthserver;

import java.util.List;
import javax.annotation.Resource;
import org.mzm.sc.oauthserver.mapper.UserMapper;
import org.mzm.sc.oauthserver.model.UserLogin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Resource
    private UserMapper userMapper;

    private int accessTokenValiditySeconds = 60 * 60;
    private int refreshTokenValiditySeconds = 7200;

    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        /**
         * authorization_code
         * http://localhost:8080/oauth/authorize?response_type=code&
         * client_id=client_1&redirect_uri=http://www.baidu.com
         **/
//        clients.inMemory().withClient("client_1").secret(passwordEncoder().encode("123456"))
//            .redirectUris("http://www.baidu.com").authorizedGrantTypes("authorization_code")
//            .scopes("all").
//            accessTokenValiditySeconds(accessTokenValiditySeconds)
//            .refreshTokenValiditySeconds(refreshTokenValiditySeconds);

        /**
         * password
         **/
        clients.inMemory().withClient("client_1").secret(passwordEncoder().encode("123456"))
            .authorizedGrantTypes("password", "client_credentials", "refresh_token").scopes("all")
            .accessTokenValiditySeconds(accessTokenValiditySeconds);
    }

    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.authenticationManager(authenticationManager())
            .allowedTokenEndpointRequestMethods(HttpMethod.GET,
                HttpMethod.POST);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
        oauthServer.allowFormAuthenticationForClients();
        oauthServer.checkTokenAccess("permitAll()");
    }

    @Bean
    AuthenticationManager authenticationManager() {
        AuthenticationManager authenticationManager = authentication -> daoAuthenticationProvider()
            .authenticate(authentication);
        return authenticationManager;
    }

    @Bean
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService());
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return daoAuthenticationProvider;
    }

    @Bean
    UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager userDetailsService = new InMemoryUserDetailsManager();

        List<UserLogin> users = userMapper.getUserList();

        for (UserLogin user : users) {
            userDetailsService
                .createUser(User.withUsername(user.getUserName())
                    .password(passwordEncoder().encode(user.getPassword()))
                    .authorities(user.getRoles() == null ? null : user.getRoles().split(","))
                    .build());
        }

        return userDetailsService;
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder;
    }
}