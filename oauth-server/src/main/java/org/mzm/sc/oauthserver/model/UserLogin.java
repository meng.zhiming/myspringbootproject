package org.mzm.sc.oauthserver.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@TableName("user_login")
@AllArgsConstructor
@NoArgsConstructor
public class UserLogin {

    private String userName;
    private String password;

    @TableField(exist = false)
    private String roles;
}
