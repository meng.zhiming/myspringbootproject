package org.mzm.sc.oauthserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Select;
import org.mzm.sc.oauthserver.model.UserLogin;

public interface UserMapper extends BaseMapper<UserLogin> {

    @Select("SELECT ul.user_name as username,\n"
        + "ul.password,\n"
        + "GROUP_CONCAT(lp.permission_name) as roles\n"
        + "FROM USER_PERMISSION up \n"
        + "inner join user_login ul\n"
        + "on (ul.id = up.user_id)\n"
        + "inner join lu_permission lp\n"
        + "on (up.permission_id = lp.id)\n"
        + "GROUP BY ul.user_name, ul.password")
    List<UserLogin> getUserList();
}
